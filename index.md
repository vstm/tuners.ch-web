---
layout: default
title: Willkommen auf tuners.ch
---

# Willkommen auf tuners.ch

Diese Webseite ist die in Hypertext verfasste, internetmässige Repräsentation
von tuners.ch. Hier ist ersichtlich was tuners.ch ist und wie man uns erreichen
kann.

## Über tuners.ch

tuners.ch ist eine Gruppe wissensinteressierter Leute, welche auf
verschiedenste Arten kommunizieren und dabei einen unterschiedlich intensiven
Wissensaustausch betreiben. Die Leute, welche an diesem Austausch teilhaben,
verfolgen dabei kein definiertes Ziel. Es gibt auch keine Regeln oder
Einschränkungen - egal welcher Art.

Neben dem reinen Austausch von Wissen gibt es ab und zu auch klägliche Versuche
ein gemeinsames Projekt auf die Beine zu stellen. Doch keines dieser Projekte
existierte länger als die anfängliche Phase der Euphorie andauerte - leider. 

Bei ganz speziellen Temperaturverhältnissen und Sternkonstellationen kommt es
unter den Mitgliedern gar zu "offiziellen" Treffs oder LAN-Parties. Ein Treffen
bei dem alle Mitglieder anwesend sind, gab es jedoch schon länger nicht mehr.

## tuners.ch Spinn-offs

Während tuners.ch selber immer einfach blieb und praktisch keine Expansion
betrieb, gab es trotzdem einige durch tuners.ch Mitglieder entstandene
"Ableger", welche durchaus Einiges zu leisten vermochten.

- [Chaostreff-AG (Aargau)](http://chaostreff-ag.tuners.ch/Hauptseite) - Das
  erste Projekt in Richtung Chaostreff
- [Chaos Computer Club Zuerich](http://ccczh.ch/) - Vormals Chaostreff Zürich

## Kontakt

Falls es wirklich jemanden da draussen gibt, welcher sich mit uns in Verbindung
setzen möchte, kann er dies auf zwei Arten anstellen:

### E-Mail

Unsere E-Mail Adresse lautet [info@tuners.ch](mailto:info@tuners.ch).

### IRC

Wir haben einen IRC-Server online, welcher mehr oder weniger gut besetzt ist.

Die koordinaten dazu lauten wie folgt:

<table>
<tr><th>Hostname:</th><td>get-a-life.ch</td></tr>
<tr><th>Port:</th><td>6697 (SSL Aktivieren!)</td></tr>
<tr><th>Channel:</th><td>#tuners</td></tr>
</table>
