# Tuners.ch Website

This is the new shit - built with [Jekyll](http://jekyllrb.com/) and [Twitter Bootstrap](http://getbootstrap.com/).

## Requirements

Shooting with cannons on birds anyone? If you just want to add new content and
don't want to modify the stylesheets you just have to [install Jekyll](http://jekyllrb.com/docs/installation/) and you're fine.

If you have to modify the [Bootstrap based] stylesheet you need to have
[less](http://lesscss.org/) installed (the lessc command needs to be in your
`$PATH`-var). If you compile the stylesheets for the first time the following
commands are also required:

- `wget` - which downloads the official Bootstrap-package
- `unzip` - to unzip said package

After that you have twitter bootstrap installed, so it won't fetch the source
again.

## Working with the site

After you have checked out this repository, you won't be able to view the site
at first. The content of the site is stored in
[Markdown](http://daringfireball.net/projects/markdown/)-files. They consist of
text (mostly) and some simple markup-elements.

### Creating the configuration for your environment

Check out the `_config.yml` which contains the configuration for the production
environment. You'll see that the `url` and `baseurl` settings are configured for
the tuners.ch server. 

You *could* now edit that file and change those two
settings to work with your environment. Then you'd have to be careful to not
commit your configuration to the repository. If I get a pull request with your
URL in the `_config.yml` I make sure you get a place in the *special* kind of
hell.

The "safer" solution is to use the fact that you can provide multiple
configuration files to Jekyll which can override the default configuration. So
create a new file, named `_config_dev.yml`. And put your `url` and `baseurl` in
there.

Let's assume you already have a local Webserver installed and you have cloned
this repository in your document root and you can access the repository with
the URL `http://mydevserver/projects/tuners.ch`. In this case your `_config_dev.yml` would look like:

    # url contains the hostname plus the http-scheme
    # no trailing slash
    url: http://mydevserver

    # the baseurl contains the path to the _site directory
    # which is by default created in the repository directory
    # again, no trailing slash.
    # If the document root of the webserver already points
    # to the _site directory, you can leave baseurl empty
    baseurl: /projects/tuners.ch/_site

If you don't have a webserver installed, you can use the built in webserver of
Jekyll. The built in webserver has also an advantage: you can pass the
`--watch`-flag to enable "Auto-regeneration". This means that Jekyll notices if
you made changes to the site and automatically rebuilds the page which has
changed.

Let's assume you want to use the built-in server and you access it using the
default settings. Your url then looks like `http://localhost:4000/`. Your
`_config_dev.yml` then looks like:

    url: http://localhost:4000
    baseurl: ""

Then you can run Jekyll with:

    jekyll serve --config _config.yml,_config_dev.yml --watch

... and in your browser open the url `http://localhost:4000`. With `jekyll help
serve` you can see the options to change the port and to which IP to bind the
server.

### Compiling the content

After you have added your preferred configuration, you can now compile the
site (well, if you don't use the `jekyll serve`-command).

If you run `jekyll build` in the "root" of the repository, the site is compiled
and the result is stored in the `_site` directory. But with that command you
build the *production* website, which most probably won't work on your
development environment.

To compiler your development build you have to run Jekyll like that:

    jekyll build --conf _config.yml,_config_dev.yml

The second configuration file should be listed in the output. Now you can open
the `_site` directory in your browser. If the stylesheet is not loaded, there
might be a problem with the `url` or `baseurl` settings and some tweaking is
required.


### Edit/Add content

Just add a new ".md"-file or edit an existing ".md"-file. If you want to see
the fruits of your labour, you have to compile the site again.

## Change the layout of the site

The HTML-output is generated using the templates in the `_layouts` directory.
If you want to just modify the HTML-part you don't need the `lessc`-stuff
mentioned in the requirements.

If you want to modify the CSS then I congratulate you to your desire to
experience a new form of pain. The following files are the most interesting
ones:

### css/style.less
The less-file which binds all of the other less-files together. This is the
"primary file" if you will.

### css/variables.less
This is our customized version of the variables.less provided by Bootstrap.  If
you want to change the colors or fonts this is the file you're looking for.

### css/custom.less
This contains our custom CSS rules. So if you want to style an element thats in
*our* HTML output, this is the file you want to edit.

There is probably no need to edit the original bootstrap files. If you do, copy
it out of the bootstrap folder and change the `@import` statement in the
`style.less`.

After your edits you have to recompile the less files to CSS. You can do this
by switching to the `css` directory and simply running `make`. As noted in the
requirements-section the Bootstrap-package is downloaded to your local computer
(I didn't want check in the whole Bootstrap packet to our source-repository).

## Help! I'm stuck

Just contact me at [stefan@tuners.ch](mailto:stefan@tuners.ch) or on the
tuners-IRC-Channel.
